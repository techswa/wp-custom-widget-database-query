<?php
/*
Plugin Name: SWA WP Primary Sponsor Widget
Plugin URI:
Description: Displays the primary sponsor's advertisement as a widget
Version: 1.0
Author: B Burt
Author URI:
License:
*/

// Exit if accessed directly.
if( !defined( 'ABSPATH' ) ) exit;

//Creating the widget
class swa_widget_primary_sponsor extends WP_Widget {

 public function __construct() {
   $widget_ops       = array('classname' => 'widget-primary-sponsor', 'description' => __('Display primary sponsor\'s widget'));
   $control_ops      = array('width' => 400, 'height' => 350);
   parent::__construct('swa_widget_sponsor', __('Primary sponsor\'s widget'), $widget_ops, $control_ops);
 }
// =========================




// ==========================

/**
 * Outputs the content of the widget
 *
 * @param array $args
 * @param array $instance
 */

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title               = apply_filters( 'widget_title', $instance['title'] );
//Manually set title in code
$title               = 'Sponsored by';

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
//echo __( 'Hello, World!', 'swa_widget_domain' );
$referrer_license    = do_shortcode('[wp_affiliate_referrer]');
$referrer_state      = do_shortcode('[wp_affiliate_referrer_details info="state"]');

//Get state
$state               = substr(strtoupper($referrer_license),0,2);

//Get type of account
if (substr(strtoupper($referrer_license),2,2) == "RE") {
  $prefix            = "RE";
} elseif (substr(strtoupper($referrer_license),2,6) == "NMLS") {
  $prefix            = "NMLS";
} else {
  $type              = "Unkown Profession";
}
echo $prefix;
//get account number
if ($prefix == "RE") {
  $account           = (substr(strtoupper($referrer_license),4));
} elseif ($prefix == "NMLS") {
  $account           = (substr(strtoupper($referrer_license),4));
}
$upload_dir_array    = wp_upload_dir();
$base_dir            = $upload_dir_array[base_dir];

$image_path          = '/ads/' . $state . '/' . $prefix . '/' . $account . '/';

$sponsor_artwork     = $referrer_license . '-sidebar-1.png';

$sponsor_ad_path     = $image_path . $sponsor_artwork;

$referrer_type       = '';

$referrer_first_name = do_shortcode('[wp_affiliate_referrer_details info="firstname"]');
$referrer_last_name  = do_shortcode('[wp_affiliate_referrer_details info="lastname"]');
$referrer_company    = do_shortcode('[wp_affiliate_referrer_details info="company"]');
$referrer_phone      = do_shortcode('[wp_affiliate_referrer_details info="phone"]');



$var .= '<div class="blue">';
$var .= $referrer_first_name . ' ' . $referrer_last_name;
$var .= '<br/>' . $referrer_company;
$var .= '<br/>' . $referrer_phone;
$var .= '</div>';

// $sponsor_ad_path ='/wp-content/uploads/tx/re/c0466958c/txrec0466958c-sidebar-1.png';

$ad_body             = '<h3>My Title</h3>';
$ad_body             = '<img src="/wp-content/uploads' . $sponsor_ad_path . '">';
//$ad_body = substr(strtoupper($referrer_license),2,2);
echo __( $ad_body, 'swa_widget_domain' );

echo $args['after_widget'];
}

// Widget Backend
/**
 * Outputs the options form on admin
 *
 * @param array $instance The widget options
 */
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title               = $instance[ 'title' ];
}
else {
$title               = __( 'New title', 'swa_widget_domain' );
}
// Widget admin form
?>
<p>
<label for
<input class
</p>
<?php
}

// Updating widget replacing old instances with new
/**
 * Processing widget options on save
 *
 * @param array $new_instance The new options
 * @param array $old_instance The previous options
 */
public function update( $new_instance, $old_instance ) {
$instance          = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function swa_load_primary_sponsor_widget() {
	register_widget( 'swa_widget_primary_sponsor' );
}
add_action( 'widgets_init', 'swa_load_primary_sponsor_widget' );
